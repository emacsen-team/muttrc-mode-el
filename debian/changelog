muttrc-mode-el (1.2.1-5) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 22:55:44 +0900

muttrc-mode-el (1.2.1-4) unstable; urgency=medium

  [ Nicholas D Steeves ]
  * Update my email address.
  * Drop myself from Uploaders.

  [ Jeremy Sowden ]
  * d/control
    - add myself to uploaders. (Closes: #1005709)
    - bump Standards-Version to 4.6.2
    - update my e-mail address
  * d/copyright
    - add myself to debian/*.
  * d/gbp.conf
    - dch: set `commit-msg`
  * d/.gitignore
    - ignore debian/ build artefacts
  * d/rules
    - make info file before `dh_installinfo`

 -- Jeremy Sowden <azazel@debian.org>  Thu, 29 Feb 2024 11:17:54 +0000

muttrc-mode-el (1.2.1-3) unstable; urgency=medium

  * Rebuild with dh-elpa 2.0.
  * Migrate to debhelper-compat 13.
  * Add Rules-Requires-Root: no.
  * Drop Enhances emacs25, because the package does not exist in bullseye.
  * Update my copyright years.
  * Declare Standards-Version: 4.5.1 (no changes required).

 -- Nicholas D Steeves <nsteeves@gmail.com>  Tue, 08 Dec 2020 06:55:51 -0500

muttrc-mode-el (1.2.1-2) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Mon, 26 Aug 2019 12:07:58 -0300

muttrc-mode-el (1.2.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/gbp.conf: Set upstream-tag to use the correct format.

 -- Nicholas D Steeves <nsteeves@gmail.com>  Wed, 28 Nov 2018 19:11:27 -0500

muttrc-mode-el (1.2+git20180915.aa1601a-1) unstable; urgency=medium

  * Initial release. (Closes: #904810)

 -- Nicholas D Steeves <nsteeves@gmail.com>  Fri, 28 Sep 2018 20:47:57 -0400
